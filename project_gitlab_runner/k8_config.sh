# Get gcloud project name - ASSUMES one project name containing playground
project_name=$(gcloud projects list --filter=playground --format="value(name)")

# Set Kubernettes cluster
gcloud container clusters get-credentials project-gitlab-runner --zone europe-west2-a --project $project_name

# Add Namespace
kubectl create ns gitlab-runner

# Create the Role
cat <<EOF | kubectl create -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: gitlab-runner
  namespace: gitlab-runner
rules:
  - apiGroups: [""]
    resources: ["pods"]
    verbs: ["list", "get", "watch", "create", "delete"]
  - apiGroups: [""]
    resources: ["pods/exec"]
    verbs: ["create"]
  - apiGroups: [""]
    resources: ["pods/log"]
    verbs: ["get"]
EOF

# Bind the Role
kubectl create rolebinding --namespace=gitlab-runner gitlab-runner-binding --role=gitlab-runner --serviceaccount=gitlab-runner:default

# Add Helm Repo
helm repo add gitlab https://charts.gitlab.io
helm repo update

# Install the helm chart
helm install --namespace gitlab-runner  gitlab-runner -f ./helm_runner/values.yaml gitlab/gitlab-runner


