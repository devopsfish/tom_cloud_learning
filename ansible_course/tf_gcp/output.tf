// A variable for extracting the external IP address of the instance
output "ip_1" {
 value = google_compute_instance.vm_1.network_interface.0.access_config.0.nat_ip
}

// A variable for extracting the external IP address of the instance
output "ip_2" {
 value = google_compute_instance.vm_2.network_interface.0.access_config.0.nat_ip
}

// A variable for extracting the external IP address of the instance
output "ip_3" {
 value = google_compute_instance.vm_3.network_interface.0.access_config.0.nat_ip
}



