// Configure the Google Cloud provider
provider "google" {
 credentials = file("../key.json")
 project     = var.project
 region      = var.location
}

resource "random_id" "instance_id_1" {
 byte_length = 8
}

// A single Compute Engine instance
resource "google_compute_instance" "vm_1" {
 name         = "ansible-vm-${random_id.instance_id_1.hex}"
 machine_type = var.machine_type
 zone         = var.location

 boot_disk {
   initialize_params {
     image = "centos-cloud/centos-7"
   }
 }

 metadata = {
   #ssh-keys = "tom:${file("~/.ssh/id_rsa.pub")}"
   ssh-keys = "terraform:${file("../.ssh/id_rsa.pub")}"
 }

// Make sure flask is installed on all new instances for later steps
 //metadata_startup_script = "sudo apt-get update; sudo apt-get install -yq build-essential python-pip rsync; pip install flask"

 network_interface {
   network = "default"

   access_config {
     // Include this section to give the VM an external ip address
   }
  }
 // Apply the firewall rule to allow external IPs to access this instance
  tags = ["http-server"]
}


resource "random_id" "instance_id_2" {
 byte_length = 8
}

// A single Compute Engine instance
resource "google_compute_instance" "vm_2" {
 name         = "ansible-vm-${random_id.instance_id_2.hex}"
 machine_type = var.machine_type
 zone         = var.location

 boot_disk {
   initialize_params {
     image = "centos-cloud/centos-7"
   }
 }

 metadata = {
   ssh-keys = "terraform:${file("../.ssh/id_rsa.pub")}"
 }


// Make sure flask is installed on all new instances for later steps
 //metadata_startup_script = "sudo apt-get update; sudo apt-get install -yq build->

 network_interface {
   network = "default"

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
 // Apply the firewall rule to allow external IPs to access this instance
  tags = ["http-server"]
}

resource "random_id" "instance_id_3" {
 byte_length = 8
}

// A single Compute Engine instance
resource "google_compute_instance" "vm_3" {
 name         = "ansible-vm-${random_id.instance_id_3.hex}"
 machine_type = var.machine_type
 zone         = var.location

 boot_disk {
   initialize_params {
     image = "centos-cloud/centos-7"
   }
 }
 metadata = {
   ssh-keys = "terraform:${file("../.ssh/id_rsa.pub")}"
 }

// Make sure flask is installed on all new instances for later steps
 //metadata_startup_script = "sudo apt-get update; sudo apt-get install -yq build->

 network_interface {
   network = "default"

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
 // Apply the firewall rule to allow external IPs to access this instance
  tags = ["http-server"]
}


