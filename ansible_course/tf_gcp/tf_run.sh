# Variable Substitution
echo Substituting Variables
envsubst < variables.tf_template > variables.tf

# Clear Old Terraform State
rm -r terraform.tfstate.d

echo Run Terraform
# Terraform Init
terraform init

# Terraform plan
terraform plan

# Terraform apply
terraform apply -auto-approve

# Get output and filter ips
terraform output | grep -E -o '(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)' > ../ansible/vm_ips




