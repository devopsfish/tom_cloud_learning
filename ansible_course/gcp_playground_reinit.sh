# Run Login
gcloud auth login

# Get gcloud project name - ASSUMES one project name containing playground
project_name=$(gcloud projects list --filter=playground --format="value(name)")

# Set Env variabe for substituion
export project_name=$project_name

# Set Active Project
echo Setting Project: $project_name

# Set Active Project
gcloud config set project $project_name

# Create Service Account
#echo Creating Service Account
gcloud iam service-accounts create tf-service --description="Terraform Service Account" --display-name="tf_service"

# Add Permission to service Account
gcloud projects add-iam-policy-binding $project_name --member=serviceAccount:tf-service@$project_name.iam.gserviceaccount.com --role=roles/editor

# Create Service Account Key
gcloud iam service-accounts keys create key.json --iam-account tf-service@$project_name.iam.gserviceaccount.com

# Create ssh keys
mkdir .ssh
cat /dev/zero | ssh-keygen -t rsa -f .ssh/id_rsa -C terraform
chmod 400 .ssh/id_rsa

# Run Terraform
cd tf_gcp
./tf_run.sh

# Generate hosts-dev
cd ../ansible
./generate_hosts-dev.sh

# Sleep
#echo sleeping for 2 Minutes
sleep 2m

# Run Playbook for http
ansible-playbook playbooks/all-playbooks.yml
