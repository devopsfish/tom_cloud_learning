# Read IPs from file
IFS=$'\n' read -d '' -r -a ips < vm_ips

# Output new file with IPs
cat <<EOF > hosts-dev
#hosts-dev
[webservers]
app1 ansible_host=${ips[0]}
app2 ansible_host=${ips[1]}

[loadbalancers]
lb1 ansible_host=${ips[2]}

[local]
control ansible_connection=local
EOF
