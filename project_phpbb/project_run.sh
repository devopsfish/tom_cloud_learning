# Infrastrcture tasks - Run Terraform
cd tf_gcp
./tf_run.sh
cd ../

# App Tasks - Run Ansible
cd ansible
./ansible_run.sh
cd ../
