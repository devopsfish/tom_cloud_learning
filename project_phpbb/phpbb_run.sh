# Create ssh keys
mkdir .ssh
cat /dev/zero | ssh-keygen -t rsa -f .ssh/id_rsa -C terraform
chmod 400 .ssh/id_rsa

# Run Terraform
cd tf_gcp
./tf_run.sh
cd ../

# Run Ansible
cd ansible
./ansible_run.sh
cd ../
