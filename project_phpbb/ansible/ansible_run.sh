# Read IPs from file
IFS=$'\n' read -d '' -r -a ips < vm_ips

# Output new file with IPs
cat <<EOF > hosts-dev
#hosts-dev
[webservers]
app1 ansible_host=${ips[0]}

[local]
control ansible_connection=local
EOF

# Read IPs from file
IFS=$'\n' read -d '' -r -a sql_ip < sql_ip

cat <<EOF > install-config.yml 
installer:
    admin:
        name: administrator
        password: administrator
        email: admin@admin.com
    board:
        lang: en
        name: phpbb Ansible Project
        description: My amazing new phpBB board
    database:
        dbms: mysqli
        dbhost: ${sql_ip[0]}
        dbport: 3306
        dbuser: phpbb_user
        dbpasswd: password
        dbname: phpbb
        table_prefix: phpbb_
    email:
        enabled: false
        smtp_delivery : ~
        smtp_host: ~
        smtp_port: ~
        smtp_auth: ~
        smtp_user: ~
        smtp_pass: ~
    server:
        cookie_secure: false
        server_protocol: http://
        force_server_vars: false
        server_name: ${ips[0]}
        server_port: 80
        script_path: /phpBB3

    extensions: ['phpbb/viglink']
EOF

# Sleep
echo sleeping for 2 Minutes
sleep 2m

# Run Playbook for http
ansible-playbook playbooks/all-playbooks.yml


