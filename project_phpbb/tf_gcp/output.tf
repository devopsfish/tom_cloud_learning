// A variable for extracting the external IP address of the instance
output "vm_ip_1" {
  value = google_compute_instance.vm_1.network_interface.0.access_config.0.nat_ip
}

output "sql_ip_1" {
  value = google_sql_database_instance.main_primary.private_ip_address
}  
