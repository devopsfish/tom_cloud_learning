# Get gcloud project name - ASSUMES one project name containing playground
project_name=$(gcloud projects list --filter=playground --format="value(name)")

# Set Env variabe for substituion
export project_name=$project_name

# Variable Substitution
echo Substituting Variables
envsubst < variables.tf_template > variables.tf

# Clear Old Terraform State
rm -r terraform.tfstate.d
rm -r terraform.tfstate
rm -r terraform.tfstate.backup 

echo Run Terraform
# Terraform Init
terraform init

# Terraform plan
terraform plan

# Terraform apply
terraform apply -auto-approve

# Get output and filter ips
terraform output | grep vm | grep -E -o '(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)' > ../ansible/vm_ips
terraform output | grep sql | grep -E -o '(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][[0-9]|[01]?[0-9][0-9]?)' > ../ansible/sql_ip



