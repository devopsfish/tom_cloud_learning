// Configure the Google Cloud provider
provider "google" {
 credentials = file("../../key.json")
 project     = var.project
 region      = var.location
}

