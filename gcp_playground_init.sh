# Run Login
gcloud auth login

# Get gcloud project name - ASSUMES one project name containing playground
project_name=$(gcloud projects list --filter=playground --format="value(name)")

# Set Env variabe for substituion
export project_name=$project_name

# Set Active Project
echo Setting Project: $project_name
gcloud config set project $project_name

# Create Service Account
echo Creating Service Account
gcloud iam service-accounts create tf-service --description="Terraform Service Account" --display-name="tf_service"

# Add Permission to service Account
gcloud projects add-iam-policy-binding $project_name --member=serviceAccount:tf-service@$project_name.iam.gserviceaccount.com --role=roles/editor
gcloud projects add-iam-policy-binding $project_name --member=serviceAccount:tf-service@$project_name.iam.gserviceaccount.com --role=roles/compute.networkAdmin

# Create Service Account Key
gcloud iam service-accounts keys create key.json --iam-account tf-service@$project_name.iam.gserviceaccount.com

# Add APIs
gcloud services enable \
    sqladmin.googleapis.com \
    cloudresourcemanager.googleapis.com \
    compute.googleapis.com \
    iam.googleapis.com \
    oslogin.googleapis.com \
    servicenetworking.googleapis.com \
    sqladmin.googleapis.com

cd project_phpbb
./phpbb_run.sh
cd ../

cd project_gitlab_runner
./runner_run.sh
